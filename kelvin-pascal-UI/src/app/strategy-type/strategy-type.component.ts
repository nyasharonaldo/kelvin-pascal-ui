import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Strategy } from '../model/strategy';

@Component({
  selector: 'app-strategy-type',
  templateUrl: './strategy-type.component.html',
  styleUrls: ['./strategy-type.component.css']
})
export class StrategyTypeComponent implements OnInit {
  strategies: Strategy[];
  strategyName: string;
  strategy: Strategy;
  stratDict = {};
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getStrategies().subscribe(
      stratsArray => {
        const sampleRowData = [];
        console.log('Successfuly retrieved strategies');
        console.log(stratsArray);
        this.strategies = stratsArray;
        this.strategy = this.strategies[0];
        for(let strat of this.strategies){
          this.stratDict[strat.name]=strat.id
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  dateFunc(date: string){
    if(date !=null){
      let x = date.split('T')
      return x[0]
    }
    return "Strategy Active"
  }



  showStats(name: string) {
    // TODO: Check for valid values
    this.apiService.getStrategyById(this.stratDict[name]).subscribe(
      strategy => {
        console.log('Successfuly retrieved strategy by name');
        console.log(strategy);
        this.strategy = strategy;
      },
      error => {
        console.log(error);
      }
    );
  }
}
