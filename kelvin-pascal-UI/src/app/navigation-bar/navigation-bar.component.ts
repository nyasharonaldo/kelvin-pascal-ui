import { Component, OnInit } from '@angular/core';
import { Strategy } from '../model/strategy';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
  activeStrategies = [];
  stratName: string;
  stratDict = {};
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getLatestActiveStrategies();
  }

  getLatestActiveStrategies(){
    this.activeStrategies = []
    this.apiService.getStrategies().subscribe(
      stratsArray => {
        console.log('Successfuly extracted strategies for active strategy list')
        for (const strat of stratsArray){
          if (strat.stop == null){
            this.activeStrategies.push(strat);
          }
          for(let strat of this.activeStrategies){
            this.stratDict[strat.name]=strat.id
          }
        }
        console.log(this.activeStrategies)
      }
    )
  }

  refresh():void{
    window.location.reload();
  }

  stopStrategy(name: string) {
    this.apiService.stopStrategy(this.stratDict[name]).subscribe(
      successful => {
        console.log("Request Successful")
        console.log("Strategy " + name + " stopped.")
        this.getLatestActiveStrategies();
      },
      error => {
        console.log(error)
      }
    );
    
  }



}
