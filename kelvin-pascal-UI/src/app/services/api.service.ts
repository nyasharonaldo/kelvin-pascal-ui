import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trade } from '../model/trade';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Stock } from '../model/stock';
import { Deal } from '../model/deal';
import { Strategy } from '../model/strategy';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public submitStrategy(name: string, ticker: string, captial: number, shortWindow: number, longWindow: number): Observable<Object[]> {
    return this.httpClient.post<Object[]>(
              environment.backendUrl + 'strategies/getUserInput/'+ name +'/' + ticker
              + '/' + captial + '/' + shortWindow+ '/'+ longWindow, {}, {});
  }

  public getStrategyById(id:number): Observable<Strategy>{
    return this.httpClient.get<Strategy>(
      environment.backendUrl + 'strategies/' + id
    )
  }

  public getStocks(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(
              environment.backendUrl + 'stocks');
  }

  public getTrades(): Observable<Trade[]> {
    return this.httpClient.get<Trade[]>(
              environment.backendUrl + 'trades');
  }

  public getDeals(): Observable<Deal[]> {
    return this.httpClient.get<Deal[]>(
              environment.backendUrl + 'deals');
  }

  public getStrategies(): Observable<Strategy[]> {
    return this.httpClient.get<Strategy[]>(
              environment.backendUrl + 'strategies');
  }

  public stopStrategy(id:number){
    let request = environment.backendUrl + 'strategies/stopping/'+id;
    console.log(request)
    return this.httpClient.put(
      request,{}
    )
  }

}