import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyInputComponent } from './strategy-input.component';

describe('StrategyInputComponent', () => {
  let component: StrategyInputComponent;
  let fixture: ComponentFixture<StrategyInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
