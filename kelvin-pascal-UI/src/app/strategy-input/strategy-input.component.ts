import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Stock } from '../model/stock';
import { Strategy } from '../model/strategy';
import { String, StringBuilder } from 'typescript-string-operations';
@Component({
  selector: 'app-strategy-input',
  templateUrl: './strategy-input.component.html',
  styleUrls: ['./strategy-input.component.css']
})
export class StrategyInputComponent implements OnInit {
  strategies: Strategy[];
  ticker: string;
  capital: number;
  shortWindowMinutes: number;
  shortWindowSeconds: number;
  longWindowMinutes: number;
  longWindowSeconds: number;
  name: string;
  stocks: Stock[]

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getStocks().subscribe(
      stocksArray => {
        console.log('Successfuly retrieved stocks');
        this.stocks = stocksArray;
      },
      error => {
        console.log(error);
      }
    );
    this.apiService.getStrategies().subscribe(
      strategiesArray => {
        this.strategies = strategiesArray;
      }
    )
  }

  isUnique(name: string){
    for (let strategy of this.strategies){
      if (name==strategy.name){
        return false;
      }
    }
    return true;
  }

  isStockSelected(){
    if (this.ticker == null){
      return false;
    }
    return true;
  }

  isCapitalValid(capital: number){
    if(capital<10000){
      return false;
    }
    return Number.isInteger(capital)
  }

  isWindowValid(minute: number, seconds: number){
    let result: number;
    if(Number.isInteger(minute) && Number.isInteger(seconds)){
      return true
    }
    return false
  }
  

  ngClick(){
    const shortWindowValid = this.isWindowValid(this.shortWindowMinutes,this.shortWindowSeconds);
    const longWindowValid = this.isWindowValid(this.longWindowMinutes,this.longWindowSeconds);
    var errorMessages = new StringBuilder("Error Message: ")
    let error = false;
    if(!this.isUnique(this.name)){
      errorMessages.Append('Name is not unique.')
      error = true;
    }
    if(this.name ==null){
      errorMessages.Append(' Name cannot be empty.')
      error = true;
    }
    if(shortWindowValid==false){
      errorMessages.Append(' Short window size is invalid (Both fields must be filled in). ')
      error = true;
    }
    if(longWindowValid==false){
      errorMessages.Append(' Long window size is invalid (Both fields must be filled in). ')
      error = true;
    }
    const shortWindowSeconds = (Number(this.shortWindowMinutes * 60) + Number(this.shortWindowSeconds));
    const longWindowSeconds = (Number(this.longWindowMinutes * 60) + Number(this.longWindowSeconds));
    if(shortWindowSeconds>longWindowSeconds){
      errorMessages.Append(' Long window size cannot be smaller than short window size. ')
      error = true;
    }
    if(this.isCapitalValid(this.capital)==false){
      errorMessages.Append(' Capital entered must be more than £10,000')
      error = true
    }
    let errorMessage = errorMessages.ToString()
 
    


    if(error ){
      alert(errorMessage);
      console.log(errorMessage);
    } else {
      console.log('Sumbit button for strategy clicked')
      console.log("Strategy Name: " + this.name + "Stock Name: " + this.ticker + " Capital: " + this.capital + " Short Window Seconds: " + shortWindowSeconds +
                  " Long Window Seconds: " + longWindowSeconds)
      this.apiService.submitStrategy(this.name, this.ticker,this.capital,shortWindowSeconds,longWindowSeconds).subscribe(
        successfulReply=>{
          alert('Strategy: ' + this.name +' successfully implemented')
          console.log(successfulReply);
          this.resetValues();
        },
        error=>{
          console.log(error)
        }
      );
    }
  }

  resetValues(){
    this.name = null;
    this.ticker = null;
    this.capital = null;;
    this.shortWindowMinutes = null;
    this.shortWindowSeconds = null;;
    this.longWindowMinutes = null;
    this.longWindowSeconds = null;

  }



}
