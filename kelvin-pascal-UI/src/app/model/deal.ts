import { Strategy } from './strategy';
import { Trade } from './trade';

export class Deal {

    constructor(
        public id: number,
        public stratergy: Strategy,
        public trade1: Trade,
        public trade2: Trade,
        public profit: number,
        public tradeType: string
      ) {}
}
