import { Stock } from './stock';

export class Strategy {
    constructor(
        public name: string,
        public capital: number,
        public currentPosition: number,
        public exitProfitLoss: number,
        public id: number,
        public longWindow: number,
        public profit: number,
        public roi: number,
        public shortWindow: number,
        public stock: Stock,
        public start: string,
        public stop: string
        ) {}
}
