import { Strategy } from './strategy';
import { Time } from '@angular/common';

export class Trade {
    constructor(
            public id: number,
            public lastStateChange: Date,
            public price: number,
            public size: number,
            public state: string,
            public stockTicker: string,
            public strategy: Strategy,
            public tradeType: string,
    ) {}
}
