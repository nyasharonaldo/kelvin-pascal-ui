import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Strategy } from '../model/strategy';

@Component({
  selector: 'app-strategies-table',
  templateUrl: './strategies-table.component.html',
  styleUrls: ['./strategies-table.component.css']
})
export class StrategiesTableComponent implements OnInit {
  strategies: Strategy[];
  columnDefs = [
    {headerName: 'Name', field: 'name', sortable: true, filter: true, width: 100 },
    {headerName: 'Ticker', field: 'ticker', sortable: true, filter: true, width: 100 },
    {headerName: 'Capital (£)', field: 'capital', sortable: true, filter: true, width: 150 },
    {headerName: 'Profit', field: 'profit', sortable: true, filter: true, width: 150 },
    {headerName: 'ROI', field: 'roi', sortable: true, filter: true, width: 75 },
    {headerName: 'Short Window', field: 'shortWindow', sortable: true, filter: true, width: 125 },
    {headerName: 'Long Window', field: 'longWindow', sortable: true, filter: true, width: 120 },
    {headerName: 'Start Date', field: 'stratStart', sortable: true, filter: true, width: 125 },
    {headerName: 'Stop Date', field: 'stratStop', sortable: true, filter: true, width: 125 }
  ];

  rowData: any[];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getStrategies().subscribe(
      stratsArray => {
        const sampleRowData = [];
        console.log('Successfuly retrieved strategies');
        console.log(stratsArray);
        this.strategies = stratsArray;
        for (const strategy of this.strategies) {
          sampleRowData.push({name: strategy.name,
                              ticker: strategy.stock.ticker,
                              capital: strategy.capital.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' }),
                              profit: strategy.profit.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' }),
                              roi: ((strategy.roi)*100).toFixed(2)+'%',
                              shortWindow: (strategy.shortWindow * 10)+'s',
                              longWindow: (strategy.longWindow * 10)+'s',
                              stratId: strategy.id,
                              stratStart: this.dateFunc(strategy.start),
                              stratStop: this.dateFunc(strategy.stop)});
        }
        this.rowData = sampleRowData;
      },
      error => {
        console.log(error);
      }
    );
  }
  dateFunc(date: string){
    if(date !=null){
      let x = date.split('T')
      return x[0]
    }
    return "Strategy Active"
  }

}
