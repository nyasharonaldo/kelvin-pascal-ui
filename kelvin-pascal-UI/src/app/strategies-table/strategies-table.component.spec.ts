import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategiesTableComponent } from './strategies-table.component';

describe('StrategiesTableComponent', () => {
  let component: StrategiesTableComponent;
  let fixture: ComponentFixture<StrategiesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategiesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategiesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
