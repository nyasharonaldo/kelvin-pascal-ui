import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Deal } from '../model/deal';

@Component({
  selector: 'app-deals-table',
  templateUrl: './deals-table.component.html',
  styleUrls: ['./deals-table.component.css']
})
export class DealsTableComponent implements OnInit {
  deals: Deal[];
  columnDefs = [
    {headerName: 'Ticker', field: 'ticker', sortable: true, filter: true, width: 100 },
    {headerName: 'Trade Type', field: 'tradeType', sortable: true, filter: true, width: 125 },
    {headerName: 'Profit', field: 'profit', sortable: true, filter: true, width: 150 },
    {headerName: 'Deal Id', field: 'id', sortable: true, filter: true, width: 100 },
    {headerName: 'Strategy Id', field: 'strategyId', sortable: true, filter: true, width: 150 },
    {headerName: 'Trade 1 Id', field: 'trade1Id', sortable: true, filter: true, width: 100 },
    {headerName: 'Trade 2 Id', field: 'trade2Id', sortable: true, filter: true, width: 100 }
  ];
  rowData = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getDeals().subscribe(
      dealsArray => {
        const sampleRowData = [];
        console.log('Deals table successfully extracted');
        console.log(dealsArray);
        this.deals = dealsArray;
        for (const deal of this.deals) {
          sampleRowData.push({ticker: deal.stratergy.stock.ticker,
                            tradeType: deal.trade1.tradeType,
                            profit: deal.profit.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' }),
                            id: deal.id,
                            strategyId: deal.stratergy.id,
                            trade1Id: deal.trade1.id,
                            trade2Id: deal.trade2.id});
        }
        this.rowData = sampleRowData;
      },
      error => {
        console.log(error);
      }
    );
  }

}
