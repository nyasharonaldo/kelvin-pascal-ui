import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StrategyInputComponent } from './strategy-input/strategy-input.component';
import { AgGridModule } from 'ag-grid-angular';
import { TradeTableComponent } from './trade-table/trade-table.component';
import { DealsTableComponent } from './deals-table/deals-table.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { FormsModule } from '@angular/forms';
import { StrategiesTableComponent } from './strategies-table/strategies-table.component';
import { StrategyTypeComponent } from './strategy-type/strategy-type.component';
import { FooterComponent } from './Features/footer/footer.component';
import { NavigationComponent } from './Features/navigation/navigation.component';
import { TemplateComponent } from './Features/template/template.component';
import { TableTabComponent } from './table-tab/table-tab.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalComponent} from './ngbd-modal/ngbd-modal.component';
@NgModule({
  declarations: [
    AppComponent,
    StrategyInputComponent,
    TradeTableComponent,
    DealsTableComponent,
    NavigationBarComponent,
    StrategiesTableComponent,
    FooterComponent,
    NavigationComponent,
    TemplateComponent,
    StrategyTypeComponent,
    TableTabComponent,
    NgbdModalComponent
    
  ],entryComponents: [NgbdModalComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
