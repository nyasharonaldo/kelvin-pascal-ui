import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Trade } from '../model/trade';

@Component({
  selector: 'app-trade-table',
  templateUrl: './trade-table.component.html',
  styleUrls: ['./trade-table.component.css']
})
export class TradeTableComponent implements OnInit {
  trades: Trade[];
  columnDefs = [
    {headerName: 'Stock', field: 'stockTicker', sortable: true, filter: true, width: 100 },
    {headerName: 'State', field: 'state', sortable: true, filter: true, width: 150  },
    {headerName: 'Size', field: 'size', sortable: true, filter: true, width: 100 },
    {headerName: 'Price', field: 'price', sortable: true, filter: true, width: 100 },
    {headerName: 'Time', field: 'lastStateChange', sortable: true, filter: true, width: 175, type: Date },
    {headerName: 'Trade Type', field: 'tradeType', sortable: true, filter: true, width: 120 },
    {headerName: 'Trade Id', field: 'id', sortable: true, filter: true, width: 100 },
    {headerName: 'Strategy Name', field: 'strategyName', sortable: true, filter: true, width: 150 },
    {headerName: 'Strategy Id', field: 'strategyId', sortable: true, filter: true, width: 125 }
  ];

  rowData = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getTrades().subscribe(
      tradesArray => {
        let x = []
        console.log('Successfuly recovered trades');
        console.log(tradesArray);
        this.trades = tradesArray;
        for (const trade of this.trades){
          x.push({
            stockTicker: trade.stockTicker,
            state: trade.state,
            size: trade.size.toLocaleString(),
            price: trade.price.toLocaleString('en-GB', { style: 'currency', currency: 'GBP' }),
            lastStateChange: this.dateTimeFunc(trade.lastStateChange.toString()),
            tradeType: trade.tradeType,
            id: trade.id,
            strategyId: trade.strategy.id,
            strategyName: trade.strategy.name
          })
          this.rowData=x;

        }
      },
      error => {
        console.log(error)
      }
    );
  }

  dateTimeFunc(date: string){
    let x = date.split('T');
    let y = (x[0]+' '+x[1]).split('.');
    
    return y[0]
  }
}
