import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-table-tab',
  templateUrl: './table-tab.component.html',
  styleUrls: ['./table-tab.component.css']
})
export class TableTabComponent implements OnInit {

  hideTab1 = false;
  hideTab2 = true;
  hideTab3 = true;

  constructor() { }

  ngOnInit() {
  }

  showTab1() {
    this.hideTab1 = false;
    this.hideTab2 = true;
    this.hideTab3 = true;
  }

  showTab2() {
    this.hideTab1 = true;
    this.hideTab2 = false;
    this.hideTab3 = true;
  }

  showTab3() {
    this.hideTab1 = true;
    this.hideTab2 = true;
    this.hideTab3 = false;
  }
}
