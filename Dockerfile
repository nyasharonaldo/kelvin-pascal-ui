FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8080

COPY kelvin-pascal-UI/dist/kelvin-pascal-UI/* /usr/share/nginx/html/
